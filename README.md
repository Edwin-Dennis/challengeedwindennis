# Agenda telefónica 

Ejercicio realizado por Edwin Dennis Santiago Marcial


## Stacks
* Sails.js 
* Mysql
* handlebars 
* S3
* Rds
* boostrap v4

## Versiones Utilizadas
* Sails 0.12.14 
* Node v6.11.3
* npm 3.10.10

## Instalación
1. Clonar el proyecto o descargarlo
2. Eliminar la carpeta node_modules (En caso de existir)
3. Posicionarse en la carpeta desde la terminal  y ejecutar 
```javascript
	                 npm install
```
(Demorara unos minutos)
4.  Ejecutar sails lift
5. Abrir la siguiente ruta en el navegador http://localhost:1338/

    
## Installation
1. Clone the project or download it
2. Delete the folder node_modules (In case of existing)
3. Position yourself in the folder from the terminal and run
```javascript
	                 npm install
```
(It was late a few minutes)

4. Run sails lift
5. Open the following path in the browser


Contacto **edwindennis@hotmail.com**

Telefono **(044)951-263-0015** 


