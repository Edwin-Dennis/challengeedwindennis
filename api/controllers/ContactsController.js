/**
 * ContactsController
 *
 * @description :: Server-side logic for managing contacts
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */



var AWS = require('aws-sdk');
AWS.config.region = 'us-east-1';
AWS.config.update({accessKeyId:'AKIAJZV3ZVLLFM4UYYNA',secretAccessKey:'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'});

module.exports = {

    get_view: function (req, res) {

        // Contacts.destroy().exec(function (err){
        //     if (err) {
        //       return res.negotiate(err);
        //     }

        //   });

        Contacts.find().exec(function (err, contacts) {
            if (err) return res.serverError(err);

            if (!contacts)
                return ResponseService.json(400, res, "no contacts found", err);

            // return ResponseService.json(200, res, "Producto Eliminado",eliminado);
            
            res.view('homepage', {
                data: contacts
            });

        });




    },

    create_contact: function (req, res) {
        let contact_to_insert = {
            nombre: req.param('nombre'),
            telefono: req.param('telefono'),
            correo: req.param('correo'),
            pathImage: 'https://s3.amazonaws.com/challengefileupload/'+req.param('pathImage')
        };

        
        Contacts.create(contact_to_insert).then(function (contact) {
            if (contact) {
                return ResponseService.json(200, res, "created contact",contact);
            }
        }).catch(function (error) {
            sails.log(error);
            if (error.code == 'E_VALIDATION') {
                return ResponseService.json(400, res, "server error", error);
            } else {
                if (error.invalidAttributes) {
                    return ResponseService.json(400, res, "invalid attributes", error);
                }
            }
        });

    },

    upload_avatar: function (req, res) {
        var nombreArchivo;
        req.file('avatar').upload({
            adapter: require('skipper-s3'),
            key: 'AKIAJZV3ZVLLFM4UYYNA',
            secret: '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',
            bucket: 'challengefileupload'
          }, function (err, filesUploaded) {
            if (err) return res.negotiate(err);
            return res.ok({
              files: filesUploaded,
              textParams: req.params.all()
            });
          });
    }

};

