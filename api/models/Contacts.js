/**
 * Contacts.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    id: {
      type: 'integer',
      autoIncrement: true,
      primaryKey: true
  },
  nombre: {
      type: 'string',
      required: true
  },
  telefono: {
      type: 'string',
      required: true
  },
  correo: {
      type: 'string',
      required: true
  },
  pathImage:{
    type: 'string',
    required: true
  }
  }
};

