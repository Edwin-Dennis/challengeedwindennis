var Tabla = (function () {


    $(document).ready(function () {



        $('#collapseDiv').collapse("toggle");
        add_contact();

    });

    var add_contact = function () {

        $('#btn_add_contact').click(function (e) {

            if ($('#frm_avatar_upload').valid()) {

                $("#frm_add_contact").validate({
                    rules: {
                        nombre: "required",
                        lada: "required",
                        telefono: "required",
                        correo: "required",

                    },
                    messages: {
                        nombre: "Ingrese un nombre",
                        lada: "Ingrese la lada",
                        telefono: "Ingrese un número válido",
                        correo: "Ingrese un correo electronico válido"

                    },
                    submitHandler: function (form) {
                    },
                    highlight: function (element) {
                        $(element).css('background', '#ffdddd');
                    },
                    unhighlight: function (element) {
                        $(element).css('background', '#ffffff');
                    }
                });


                if ($("#frm_add_contact").valid()) {


                    var formData = new FormData();
                    formData.append('nombre', 'edwin');
                    formData.append('action', 'hola');
                    // Attach file
                    formData.append('avatar', $('input[type=file]')[0].files[0]);
                    var pathReturn = "";
                    $.ajax({
                        url: '/upload_avatar',
                        data: formData,
                        type: "POST",
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            pathReturn = data.files[0].fd;
                            let new_contact = {
                                nombre: $('#nombre').val().toUpperCase(),
                                lada: $('#lada').val(),
                                telefono: $('#telefono').val().toUpperCase(),
                                correo: $('#correo').val(),
                                pathImage: pathReturn
                            };


                            $.post("/create_contact", new_contact)
                                .done(function (data) {
                                    let response=data.response.data;
                                    $('#container_contacts').append(                                    '<div class="row item " data-toggle="collapse" href="#collapseExample'+($("#container_contacts" ).length+1)+'" aria-expanded="false" aria-controls="collapseExample'+$("#container_contacts" ).length+1+'">'+
                                    '<div class="form-control">'+
                                        '<div class="row">'+
                                            '<img src="'+response.pathImage+'" alt="imagen avatar" class=" col-md-2 avatar">'+
                                            '<h4 class="text-justify col-md-9">'+response.nombre+'</h4>'+
                                        '</div>'+
                                        '<div class="collapse" id="collapseExample'+($("#container_contacts" ).length+1)+'">'+
                                            '<div class="card card-body">'+
                                                '<div class="row">'+
                                                    '<h6 class="col-md-5">Telefono:'+response.telefono+'</h6>'+
            
                                                    '<h6 class="col-md-7">Correo:'+response.correo+'</h6>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>');

                                $('#frm_add_contact')[0].reset();
                                $('#frm_avatar_upload')[0].reset();
                                
                                    
                                })
                                .fail(function () {

                                });

                        }, error: function (err) {
                            console.log("error", err);
                        }
                    });

                }
            }


        })
    }

    return {

    };

})();



